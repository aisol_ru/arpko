<?php

namespace app\widgets\back_call_popup;

use yii\base\Model;

class BackCallForm extends Model
{

    public $name;
    public $phone;

    public function rules()
    {
        return [
            ['name', 'required', 'message' => 'Введите ФИО'],
            ['phone', 'required', 'message' => 'Введите номер телефона'],
        ];
    }

}
