<?php foreach ($items as $item) { ?>

                    <?php if (empty($item->link)) { ?>
                        <div class="image">
                            <img src="<?= $item->getResizePath('file', 425, 115) ?>" alt="<?= $item->name ?>" class="animate-scale">
                        </div>
                        <div class="title">
                            <span><?= $item->name ?></span>
                        </div>
                    <?php } else { ?>
                        <a href="<?= $item->link ?>">
                            <div class="image">
                                <img src="<?= $item->getResizePath('file', 425, 115) ?>" alt="<?= $item->name ?>" class="animate-scale">
                            </div>
                            <div class="title">
                                <span><?= $item->name ?></span>
                            </div>
                        </a>
                    <?php } ?>
<?php } ?>
