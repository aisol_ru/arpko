<?php if (count($items) > 0) { ?>
<!--    <section class="slick" >-->
<!--    <div class="main-slide slick-slide slick-current slick-center" role="listbox" style="opacity: 1; width: 20000px; transform: translate3d(-4822px, 0px, 0px);">-->
        <div class="tp-banner-container hidden-xs" id="slick_main_<?= $id ?>">
            <?php foreach ($items as $item) { ?>
                <div>
                    <img src="<?= $item->getPath('image') ?>"/>
                </div>
                <div>
                    <?= $item->content ?>
                </div>
            <?php } ?>
        </div>
<!--    </div>-->
<!--    </section>-->
<?php } ?>