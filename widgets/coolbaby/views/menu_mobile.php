<?php

use app\components\IcmsHelper;
use app\models\Parameter;
use app\models\Tree;
?>


<div class="slide-menu">

    <div class="wrapper-bg">

        <div class="slide-menu-content">

            <div class="logo-wrapper">
                <a href="http://arpko.a-test.ru/"><div class="logo"></div></a>
            </div>

            <ul class="menu-mobile" id="yw0">

                <?php foreach ( $pages as $key => $page ) { ?>

                    <li><a href="<?= $page->url ?>"><?= $page->name_menu ?></a>

                        <ul class="dropdown">

                            <?php foreach ($page->getPages()->andWhere(['status' => Tree::STATUS_ACTIVE, 'in_menu' => 1])->orderBy(['sort' => SORT_ASC])->each() as $child) { ?>

                                <li><a href="<?= $child->url ?>"><?= $child->name_menu ?></a></li>

                            <?php } ?>

                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>

        <a class="close-menu" href="javascript:void(0);"></a>
    </div>
</div>