<?php

use app\components\IcmsHelper;
use app\models\Parameter;
use app\models\Tree;
?>

<nav>
    <ul class="head-menu" id="yw1">

        <?php foreach ( $pages as $key => $page ) {
            $is_active = false;
            if (\Yii::$app->urlManager->pageId == $page->id){
                $is_active = true;
            }

            ?>
            <li
            <?php if ($is_active) {?>
                class="active"
            <?php } ?>
            ><a href="<?= $page->url ?>"><?= $page->name_menu ?></a>

                <div class="dropdown-menu-wrapper" style="top: 78px;">
                    <ul class="dropdown">

                        <?php

                        foreach ($page->getPages()->andWhere(['status' => Tree::STATUS_ACTIVE, 'in_menu' => 1])->orderBy(['sort' => SORT_ASC])->each() as $child) { ?>

                            <li><a href="<?= $child->url ?>"><?= $child->name_menu ?></a></li>

                        <?php } ?>
                    </ul>
                </div>
            </li>
        <?php } ?>

    </ul>
</nav>




