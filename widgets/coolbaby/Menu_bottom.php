<?php

namespace app\widgets\coolbaby;

use yii\base\Widget;
use app\models\Tree;

class Menu_bottom extends Widget
{

    public function run()
    {
        $pages = Tree::find()->andWhere(['status' => Tree::STATUS_ACTIVE, 'in_menu' => 1])->orderBy(['sort' => SORT_ASC])->all();

        if (!empty($pages)) {
            return $this->render('menu_bottom', [
                        'pages' => $pages
            ]);
        }
    }

}
