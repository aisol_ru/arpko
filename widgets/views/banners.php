<?php if (!empty($items)) { ?>
    <div class="row margin-bottom-60">
        <?php
        $cnt = 0;
        foreach ($items as $item) {
            ?>
            <div class="col-md-6 md-margin-bottom-30">
                <div class="overflow-h">
                    <div>
                        <?php if (!empty($item->file)) { ?>
                        <a href="<?= $item->link ?>">
                            <img src="<?= $item->getPath('file') ?>" alt="<?= $item->name ?>">
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
<?php } ?>