<a href="/basket">
    <span class="mini-basket-price">
        <?= number_format($price, 0, '.', ' ') ?>
    </span>
    р.
    <i class="fa fa-shopping-cart"></i>
</a>
<span class="badge badge-sea rounded-x mini-basket-count"><?= $count ?></span>