<?php
if (count($items) > 0) {
    ?>

    <div class="slickie">


    <div class="slick" id="slick_<?= $id ?>">
        <?php foreach ($items as $item) { ?>
            <div>
                <a href="<?=$item->link?>">
                <img src="<?= $item->getPath('image') ?> "/>
                    <span class="ms-text-block"><?= $item->content ?></span>
                </a>
            </div>
        <?php } ?>
    </div>
    </div>
    <?php

}