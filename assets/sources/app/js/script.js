$(document).ready(function(){
    var headMenu = $('.head-menu');
    $(window).resize(function(){   
        var documentWidth = $(document).width();
        $('.head-menu .dropdown-menu-wrapper').css({
            //left: -(documentWidth - headMenu.outerWidth()) / 2,
            top: headMenu.outerHeight()
        });

    });

    $('.menu-mobile > li > a').click(function() {
        var parent = $(this).parent();
        if (!parent.hasClass('active') && parent.find('.dropdown').length) {
            parent.addClass('active').siblings().removeClass('active');
            return false;
        }
    });


    $(function(){
        $("#phone").mask("8(999) 999-9999");
    });

    $('.btn-menu-slide').click(function() {
        $('.slide-menu').addClass('active');
        $('.wrapper, .footer').addClass('active-blur');
    });

    $('.close-menu').click(function() {
        $('.slide-menu').removeClass('active');
        $('.wrapper, .footer').removeClass('active-blur');
    });


    $('.btn-show-search').click(function() {
        $('.search').addClass('active-search');
    });

    $('.btn-close-search').click(function() {
        $('.search').removeClass('active-search');
    });

    $('.fulltext table').wrap('<div class="mobile-horizontal-scroll"></div>');



});

// $(window).load(function() { $(window).trigger('resize'); }  );
/*
     FILE ARCHIVED ON 18:10:30 Mar 29, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 06:47:43 Oct 16, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 33.834 (3)
  exclusion.robots.policy: 0.219
  captures_list: 55.202
  exclusion.robots: 0.234
  esindex: 0.016
  RedisCDXSource: 4.54
  PetaboxLoader3.datanode: 68.034 (5)
  PetaboxLoader3.resolve: 130.138 (2)
  load_resource: 178.09
  CDXLines.iter: 13.431 (3)
*/