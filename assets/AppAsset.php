<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{

    public $sourcePath = '@app/assets/sources/app';
    
    public $css = [

        'css/slick.css',
//        'css/banner-styles.css',
        'css/fonts.css',
        'css/iconochive.css',
        'css/jquery.fancybox.css',
        'css/main.css',
        'css/media.css',

        'core/main.css',
        'daygrid/main.css',
        'css/media.css',



    ];
    public $js = [

        'js/ait-client-rewrite.js',
        'js/analytics.js',
        'js/auto-complete.js',
        'js/cnt.js',
        'js/graph-calc.js',
        'js/jQuery.scrollSpeed.js',
        'js/jquery.sticky-kit.min.js',
        'js/jquery-ui.js',
        'js/datepicker-ru.js',
        'js/jquery.maskedinput.min.js',
        'js/script.js',
        'js/select2.min.js',
        'js/timestamp.js',
        'js/toolbar.js',
        'js/watch.js',
        'js/wbhack.js',
        'js/slick.js',

        'core/main.js',
        'daygrid/main.js',
        'core/locales/ru.js',
        'interaction/main.js',
//        'js/tooltip.js',
//        'js/propper.js',

    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\ShopAsset',
        'app\assets\GrowlAsset',
        'app\assets\ToTopAsset',
        'app\assets\FancyBoxAsset',
    ];

    public static function path($relativePath = '')
    {
        $obj = new self();
        return \Yii::$app->assetManager->getPublishedUrl($obj->sourcePath) . '/' . $relativePath;
    }

    public function init()
    {
        parent::init();

        if (YII_DEBUG && !\Yii::$app->request->isPjax) {
            $this->publishOptions['forceCopy'] = true;
        }
        $this->addIe9Style();
    }

    public function addIe9Style()
    {
        $view = \Yii::$app->getView();
        $manager = $view->getAssetManager();
        $publishUrl = $manager->getPublishedUrl($this->sourcePath);
        $view->registerJsFile($publishUrl . 'js/html5shiv.js', ['condition' => 'lt IE 9']);
        $view->registerJsFile($publishUrl . 'js/respond.min.js', ['condition' => 'lt IE 9']);
    }

}
