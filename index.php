<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<?php
use app\assets\AppAsset;
use yii\widgets\LinkPager;
use app\components\IcmsHelper;
use app\controllers;
use app\models\Catalog;
use app\models\CatalogCategorie;
use app\models\CatalogProp;
use app\models\CatalogSku;
use app\models\CatalogSkuPropsValue;
use app\forms\SearchForm;
use app\models\ContentCategorie;
use app\models\Gallery;
use app\models\GalleryCategorie;
use app\models\Prop;
use app\models\Content;
use yii\helpers\ArrayHelper;
use app\models\Tree;
use yii\data\Pagination;
use app\components\controller;
use yii\helpers\Url;
use yii\base\Widget;
use yii\helpers\Html;
use app\models\Slide;
use app\widgets\slick\SlickAssets;
?>
<?//= $ABS1 ?>
    <div class="wrapper">

        <div class="header">
            <div class="search">
                <form>
                    <input type="text" name="search" placeholder="Введите текст" autocomplete="off">
                    <input class="btn-submit-search" type="button" name="find">
                    <input class="btn-close-search" type="button" name="close">
                </form>
            </div>

            <div class="feedback-header">
                <div class="btn-menu-slide" onclick="javascript:void(0);"></div>
                <div class="social-circles mobile">
                    <!-- <span class="circle phone"></span> -->
                    <a href="https://www.facebook.com/bckostroma44/" target="_blank"><span class="circle fb"></span></a>
                    <a href="http://vk.com/bckostroma" target="_blank"><span class="circle vk"></span></a>
                    <span class="circle find btn-show-search"></span>
                    <a href="http://www.arp-ko.ru/special"><span class="circle eye"></span></a>
                </div>
            </div>

            <div class="feddback-head">
                <div class="logo"><a class="logo-link" href="<?=  Url::to(['site/index']) ?>"></a></div>
                <div class="adress">
                    <p class="address-phone"><a href="tel:+74942422092">+7 (4942) 422-092</a></p>
                    <p>г. Кострома, ул. Локомотивная, 2</p>
                </div>
            </div>

            <div class="social-circles">
                <!-- <span class="circle phone"></span> -->
                <a href="https://www.facebook.com/bckostroma44/" target="_blank"><span class="circle fb"></span></a>
                <a href="http://vk.com/bckostroma" target="_blank"><span class="circle vk"></span></a>
                <span class="circle find btn-show-search"></span>
                <a href="http://www.arp-ko.ru/special"><span class="circle eye"></span></a>
            </div>

            <div class="feddback-head-two">
                <div class="just-strange-logo"><img src="<?= AppAsset::path('img/my_business.png') ?>" alt=""></div>
                <div class="logo-two"><a class="logo-link" href="http://www.arp-ko.ru/ric44.ru"></a></div>
                <div class="adress">

                    <p class="address-phone"><a href="tel:+74942423583"><span>Горячая линия</span><br>+7 (4942) 423-583</a></p>
                    <p>г. Кострома, ул. Локомотивная, 2, оф. 5</p>
                </div>
            </div>

            <div class="clear"></div>

            <?= app\widgets\coolbaby\Menu_main::widget() ?>

        </div>


        <div class="wide-wrapper">

            <?= \app\widgets\slick\Slick::widget(['id' => 1]) ?>

        </div>

        <div class="gos_uslugi">
            <a href="https://www.gosuslugi.ru/" target="_blank"><img src="<?= AppAsset::path('img/gos_uslugi.svg') ?>" class="logo-uslugi"></a>
            <span>
        <a href="http://www.arp-ko.ru/uploads/files/InstruktsiyaporegistratsiidlyaIOGV3.pdf">Инструкция по регистрации на портале гос. услуг</a>
        <a href="http://www.arp-ko.ru/preimuschestva-predostavleniya-gos-uslug-v-elektronnom-vide">Преимущества предоставления гос. услуг в электронном виде</a>
    </span>
        </div>

        <div class="sub-slider">

            <div class="sub-slider-wrapper">

                <div class="block first">
                    <a href="http://www.arp-ko.ru/ric44.ru">
                                            <span class="img_block">
                        <img src="<?= AppAsset::path('img/190.jpg') ?>" alt="">
                        </span>
                        <p>Оказание информационно-консультационных услуг для предпринимателей</p>
                    </a>
                </div>
                <div class="block">
                    <a href="http://www.arp-ko.ru/napravleniya/sodeystvie-v-poluchenii-kreditov">
                                            <span class="img_block">
                        <img src="<?= AppAsset::path('img/191.jpg') ?>" alt="">
                        </span>
                        <p>Содействие в получении кредитов</p>
                    </a>
                </div>
                <div class="block">
                    <a href="http://www.arp-ko.ru/napravleniya/molodezhnoe-predprinimatelstvo">
                                            <span class="img_block">
                        <img src="<?= AppAsset::path('img/192.jpg') ?>" alt="">
                        </span>
                        <p>Молодежное предпринимательство</p>
                    </a>
                </div>
                <div class="block">
                    <a href="http://www.arp-ko.ru/napravleniya/granty-subsidii-predpriyatiyam">
                                            <span class="img_block">
                        <img src="<?= AppAsset::path('img/193.jpg') ?>" alt="">
                        </span>
                        <p>Гранты, субсидии предприятиям</p>
                    </a>
                </div>
                <div class="block">
                    <a href="http://www.arp-ko.ru/napravleniya/seminary">
                                            <span class="img_block">
                        <img src=<?= AppAsset::path('img/194.jpg') ?>" alt="">
                        </span>
                        <p>Бесплатное обучение</p>
                    </a>
                </div>
                <div class="block last">
                    <a href="http://investkostroma.ru/" target="_blank">
                <span class="img_block">
                    <img src="<?= AppAsset::path('img/193.jpg') ?>" alt="">
                </span>
                        <p>Инвестиционный портал КО</p>
                    </a>
                </div>

            </div>

        </div>

        <div class="bns-panel content">

            <?= app\widgets\Banners::widget([

                'banner_group_id'=> 2

            ]) ?>


        </div>


        <div class="content">

            <div class="news-line">

                <h3 class="wrapper-title news-tit">новости</h3>
                <?= $content ?>
                <div class="btn-item-submit all-news">
                    <a href="http://arpko.a-test.ru/novosti" class="vote-submit">Показать все новости</a>
                </div>
            </div>

            <div class="poll">
                <h3 class="wrapper-title poll-tit">Опрос</h3>
                <p class="question">Оцените существующие в Костромской области условия для ведения предпринимательской деятельности по 5-бальной шкале (1 - максимально отрицательная оценка, 5 – максимально положительная оценка). </p>
                <p></p>
                <p class="answer">Ваш голос учтён. Спасибо.</p>
            </div>

        </div>


        <div class="content_date">

            <div class="news-calendar">

                <div id='calendar'></div>

            </div>

        </div>


        <div class="bns-panel content">

            <?= app\widgets\Banners::widget([

                'banner_group_id'=> 1

            ]) ?>

        </div>

        <div class="push"></div>


    </div>
    <script>

        document.addEventListener('DOMContentLoaded', function() {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                locale: 'ru',
                plugins: [ 'dayGrid', 'timeGrid', 'list', 'bootstrap', 'interaction' ],
                themeSystem: 'bootstrap',
                defaultView: 'dayGridMonth',
                validRange: {
                    start: '2019-05-01',
                    end: '2020-05-01'
                },
                selectable: true,

                dateClick: function(info) {
                    alert('Clicked on: ' + info.dateStr);

                    alert('Current view: ' + info.view.type);
                },
                customButtons: {
                    myCustomButton: {
                        text: 'custom!',
                        click: function() {
                            alert('clicked the custom button!');
                        }
                    }
                },
                prev: 'left-single-arrow',
                next: 'right-single-arrow',
                header: {
                    left: 'prev, myCustomButton',
                    right: 'next',
                    center: 'title',

                    // right: 'dayGridMonth,timeGridWeek,timeGridDay'
                },
                weekNumbers: false,
                eventLimit: false, // allow "more" link when too many events
                events: [

                    {
                        title: "All Day Event",
                        start: '2019-09-01'
                    },
                    {
                        title: 'Long Event',
                        start: '2019-09-07',
                        end: '2019-09-10'
                    },
                    {
                        groupId: '999',
                        title: 'Repeating Event',
                        start: '2019-09-09T16:00:00'
                    },
                    {
                        groupId: '999',
                        title: 'Repeating Event',
                        start: '2019-09-16T16:00:00'
                    },
                    {
                        title: 'Conference',
                        start: '2019-09-11',
                        end: '2019-09-13'
                    },
                    {
                        title: 'Meeting',
                        start: '2019-09-12T10:30:00',
                        end: '2019-09-12T12:30:00'
                    },
                    {
                        title: 'Lunch',
                        start: '2019-09-12T12:00:00'
                    },
                    {
                        title: 'Meeting',
                        start: '2019-09-12T14:30:00'
                    },
                    {
                        title: 'Birthday Party',
                        start: '2019-09-13T07:00:00'
                    },
                    {
                        title: 'Click for Google',
                        url: 'http://google.com/',
                        start: '2019-09-28'
                    }
                ]


            });

            calendar.render();
        });

    </script>

<?php $this->endContent(); ?>