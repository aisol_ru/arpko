
<?php
use yii\widgets\LinkPager;
use app\components\IcmsHelper;
?>

    <?php if (!empty($contents)) { ?>
        <div>
            <?= $this->tree->getContent() ?>
        </div>

        <?php foreach ($contents as $cont) { ?>

            <div class="post-item">
                <div class="data-post"><?= IcmsHelper::dateTimeFormat('d/m <br> Y', $cont->g_date) ?></div>
                <img src="<?= $cont->getPath('image') ?>" alt="<?=$cont->name?>">
                <a href="<?= yii\helpers\Url::to(['site/news_element', 'alias' => $cont->alias]) ?>" class="post-link">
                    <p class="title"><?= $cont->name ?></p>
                    <p><?= $cont->anons ?></p>
                    <hr class="line-end">
                </a>
            </div>

        <?php } ?>
        <?= LinkPager::widget([
            'pagination' => $pages,
        ]);
    } else { ?>
        <div class="alert alert-info fade in">
            <strong>Список новостей пуст</strong>
        </div>
    <?php } ?>
<?php
