<?php
/* @var $this app\components\View */

use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets\coolbaby\BasketMini;
use yii\widgets\ActiveForm;
use app\forms\SearchForm;
use app\models\Parameter;
use yii\widgets\LinkPager;
use app\components\IcmsHelper;
use app\controllers;
use app\models\Catalog;
use app\models\CatalogCategorie;
use app\models\CatalogProp;
use app\models\CatalogSku;
use app\models\CatalogSkuPropsValue;

use app\models\ContentCategorie;
use app\models\Gallery;
use app\models\GalleryCategorie;
use app\models\Prop;
use app\models\Content;
use yii\helpers\ArrayHelper;
use app\models\Tree;
use yii\data\Pagination;
use app\components\controller;
use yii\helpers\Url;
use yii\base\Widget;

AppAsset::register($this);//для подключения стилей
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <!--[if IE]>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>

    <body>

    <?php $this->beginBody() ?>

    <?= app\widgets\coolbaby\Menu_mobile::widget() ?>

    <?= $content ?>

    <div class="footer">

            <div>

                <ul class="menu-footer" id="yw2">

                    <?= app\widgets\coolbaby\Menu_bottom::widget() ?>

                </ul>

            </div>

            <div class="footer-content">
                <div class="logo-white"><a class="logo-link" href="<?=  Url::to(['site/index']) ?>"></a></div>
                <div class="feedback-header">
                    <div class="adress">
                        <p class="address-phone"><a href="tel:+74942422092">+7 (4942) 422-092</a></p>
                        <p>г. Кострома, ул. Локомотивная, 2</p>
                    </div>
                    <div class="social-circles">
                        <!-- <span class="circle phone"></span> -->
                        <a href="https://www.facebook.com/bckostroma44/" target="_blank"><span class="circle white-fb"></span></a>
                        <a href="http://vk.com/bckostroma" target="_blank"><span class="circle white-vk"></span></a>
                        <a href="http://www.arp-ko.ru/special"><span class="circle white-eye"></span></a>
                    </div>
                </div>

        </div>

    </body>

    <?php $this->endBody() ?>


</html>


<?php $this->endPage() ?>