<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<?php use app\assets\AppAsset;

use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

use yii\base\Widget;
use yii\helpers\Html;
?>


        <div class="wrapper">
                <div class="header">
                        <div class="search">
                                <form>
                                        <input type="text" name="search" placeholder="Введите текст" autocomplete="off">
                                        <input class="btn-submit-search" type="button" name="find">
                                        <input class="btn-close-search" type="button" name="close">
                                </form>
                        </div>
                        <div class="feedback-header">
                                <div class="btn-menu-slide" onclick="javascript:void(0);"></div>
                                <div class="social-circles mobile">
                                        <!-- <span class="circle phone"></span> -->
                                        <a href="https://www.facebook.com/bckostroma44/" target="_blank"><span class="circle fb"></span></a>
                                        <a href="http://vk.com/bckostroma" target="_blank"><span class="circle vk"></span></a>
                                        <span class="circle find btn-show-search"></span>
                                        <a href="http://www.arp-ko.ru/special"><span class="circle eye"></span></a>
                                </div>
                        </div>
                        <div class="feddback-head">
                                <div class="logo"><a class="logo-link" href="<?=  Url::to(['site/index']) ?>"></a></div>
                                <div class="adress">
                                        <p class="address-phone"><a href="tel:+74942422092">+7 (4942) 422-092</a></p>
                                        <p>г. Кострома, ул. Локомотивная, 2</p>
                                </div>
                        </div>
                        <div class="social-circles">
                                <!-- <span class="circle phone"></span> -->
                                <a href="https://www.facebook.com/bckostroma44/" target="_blank"><span class="circle fb"></span></a>
                                <a href="http://vk.com/bckostroma" target="_blank"><span class="circle vk"></span></a>
                                <span class="circle find btn-show-search"></span>
                                <a href="http://www.arp-ko.ru/special"><span class="circle eye"></span></a>
                        </div>
                        <div class="feddback-head-two">
                                <div class="just-strange-logo"><img src="<?= AppAsset::path('img/my_business.png') ?>" alt=""></div>
                                <div class="logo-two"><a class="logo-link" href="http://www.arp-ko.ru/ric44.ru"></a></div>
                                <div class="adress">

                                        <p class="address-phone"><a href="tel:+74942423583"><span>Горячая линия</span><br>+7 (4942) 423-583</a></p>
                                        <p>г. Кострома, ул. Локомотивная, 2, оф. 5</p>
                                </div>
                        </div>
                        <div class="clear"></div>
                        <?= app\widgets\coolbaby\Menu_main::widget() ?>

                </div>

                <div class="content">
                        <div class="breadcrumbs">
                                <?= Breadcrumbs::widget(
                                    [
                                        'links' => \Yii::$app->controller->bread,
                                        'activeItemTemplate' => '{link}',
                                        'options' => ['class' => 'container'],
                                        'itemTemplate' => '{link}<span class="divider">&nbsp;</span>',
                                        'tag' => 'div'
                                    ]
                                ) ?>
                        </div>

                        <div class="fullnews">
                                <div class="head-title">
                                        <h1 class="fullnews-title"><?= $this->h1 ?></h1>
                                </div>
                                <?= $content ?>
                        </div>
                </div>
                <div class="push"></div>
        </div>

<?php $this->endContent(); ?>
