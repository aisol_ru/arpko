<?php

namespace app\forms;

use yii\base\Model;

class SearchForm extends Model
{

    public $searchText;

    public function rules()
    {
        return [
            [['searchText'], 'required', 'message' => 'Введите запрос для поиска'],
            [['searchText'], 'safe'],
        ];
    }

}
